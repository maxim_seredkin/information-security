﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace security_information.Models.Hasher
{
    /// <summary>
    /// Класс хэшер SHA-1
    /// </summary>
    public class SHA_1Hasher : IHasher
    {
        /// <summary>
        /// Вектор инициализации
        /// </summary>
        private uint h0 = 0x67452301,
                     h1 = 0xEFCDAB89,
                     h2 = 0x98BADCFE,
                     h3 = 0x10325476,
                     h4 = 0xC3D2E1F0;

        /// <summary>
        /// Функция побитового циклического сдвига влево
        /// </summary>
        /// <param name="input">входящее 32-битное слово</param>
        /// <param name="n">размер сдвига</param>
        /// <returns></returns>
        private uint S(uint input, int n)
        {
            return ((input << n) | (input >> (32 - n)));
        }

        /// <summary>
        /// Раундовая функция f
        /// </summary>
        /// <param name="t">номер раунда</param>
        /// <param name="b">переменная b</param>
        /// <param name="c">переменная c</param>
        /// <param name="d">переменная d</param>
        /// <returns></returns>
        private uint f(int t, uint b, uint c, uint d)
        {
            if ((0 <= t) && (t <= 19))
                return (b & c) | ((~b) & d);
            else if ((20 <= t) && (t <= 39))
                return b ^ c ^ d;
            else if ((40 <= t) && (t <= 59))
                return (b & c) | (b & d) | (c & d);
            else if ((60 <= t) && (t <= 79))
                return b ^ c ^ d;

            throw new NotImplementedException();
        }

        /// <summary>
        /// рундовая константа k
        /// </summary>
        /// <param name="t">номер раунда</param>
        /// <returns></returns>
        private uint K(int t)
        {
            if ((0 <= t) && (t <= 19))
                return 0x5A827999;
            else if ((20 <= t) && (t <= 39))
                return 0x6ED9EBA1;
            else if ((40 <= t) && (t <= 59))
                return 0x8F1BBCDC;
            else if ((60 <= t) && (t <= 79))
                return 0xCA62C1D6;

            throw new NotImplementedException();
        }

        /// <summary>
        /// Функция инициализации сообщения - перевод в 32-битные слова и дополнение
        /// </summary>
        /// <param name="input">входящее сообщение</param>
        /// <returns></returns>
        private List<uint> Initialize(ref String input)
        {
            List<uint> result = new List<uint>();
            uint temp = 0x0;

            for (int i = 1; i <= input.Length; ++i) {
                temp |= (byte)input[i - 1];

                if (i % 4 == 0) {
                    result.Add(temp);
                    temp = 0x0;
                }
                temp <<= 8;
            }

            if (input.Length % 4 != 0) {
                temp |= 0x80;
                temp <<= (3 - input.Length % 4) * 8;
            }

            result.Add(temp);
            result.AddRange(new uint[14 - result.Count % 16]);
            UInt64 size = (UInt64)((input.Length * 8));
            result.AddRange(new uint[] { (uint)(size >> 32), (uint)size });

            //if (result.Count / 16 == 1)
            //    result.InsertRange(result.Count - 2, new uint[16]);

            return result;
        }

        /// <summary>
        /// Функция хеширования
        /// </summary>
        /// <param name="input">входящее сообщение</param>
        /// <returns></returns>
        public String hash(String input)
        {

            List<uint> words = Initialize(ref input);

            for (int i = 0, end = words.Count / 16; i < end; ++i) {
                List<uint> W = words.GetRange(i * 16, 16);

                for (int t = 16; t <= 79; ++t)
                    W.Add(S((W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]), 1));

                uint a = h0,
                    b = h1,
                    c = h2,
                    d = h3,
                    e = h4,
                    temp = 0;

                for (int t = 0; t <= 79; ++t) {
                    temp = S(a, 5) + f(t, b, c, d) + e + K(t) + W[t];
                    e = d;
                    d = c;
                    c = S(b, 30);
                    b = a;
                    a = temp;
                }

                h0 = h0 + a;
                h1 = h1 + b;
                h2 = h2 + c;
                h3 = h3 + d;
                h4 = h4 + e;
            }

            return Convert.ToString(h0, 16).PadLeft(8, '0') + " " + Convert.ToString(h1, 16).PadLeft(8, '0') + " " + Convert.ToString(h2, 16).PadLeft(8, '0') + " " + Convert.ToString(h3, 16).PadLeft(8, '0') + " " + Convert.ToString(h4, 16).PadLeft(8, '0');
        }
    }
}