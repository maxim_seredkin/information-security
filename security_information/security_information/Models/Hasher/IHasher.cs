﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace security_information.Models.Hasher
{
    /// <summary>
    /// Интерфейс хэшер
    /// </summary>
    interface IHasher
    {
        /// <summary>
        /// Функция хеширования
        /// </summary>
        /// <param name="input">входящее сообщение</param>
        /// <returns></returns>
        String hash(String input);
    }
}
