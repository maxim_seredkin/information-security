﻿using System.Collections.Generic;
using System.Web.Mvc;
using security_information.Models;
using security_information.Models.DSA;

namespace security_information.Controllers
{
    [RoutePrefix("ecdsa")]
    public class ECDSAController : Controller
    {
        // GET: ECDSA
        public ActionResult Index()
        {
            return View();
        }

        [Route("dsa")]
        [HttpPost]
        public ActionResult Encript(int hash, int a, int b, int n)
        {
            IDSA dsa = new ECDSA();
            return Json(dsa.DSA(hash, a, b, n));
        }

        [Route("verify")]
        [HttpPost]
        public ActionResult Decript(int hash, int a, int b, int n, int q, int px, int py, int qx, int qy, int r, int s)
        {
            IDSA dsa = new ECDSA();
            return Json(dsa.VerificationDSA(hash, a, b, n, q, px, py, qx, qy, r, s));
        }

        [Route("test")]
        [HttpGet]
        public ActionResult Test()
        {
            IDSA dsa = new ECDSA();
            var curves = new List<EllipticCurve>();
            var curve = EllipticCurve.Builder.Build(1, 28, 71);
            curve.RangeECWithInclude();
            return Json(curve, JsonRequestBehavior.AllowGet);
        }
    }
}