﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("des-ecb")]
    public class DESECBController : Controller
    {
        // GET: DESECB
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="open_message"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", String key = "")
        {
            IEncryptor encryptor = new DESECBEncryptor();
            return "[ 0, "+encryptor.encrypt(open_message, new object[] { key }) + " ]";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enc_message"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", String key = "")
        {
            IEncryptor encriptor = new DESECBEncryptor();
            return "[ 0, "+encriptor.decrypt(enc_message, new object[] { key }) + " ]";
        }
    }
}