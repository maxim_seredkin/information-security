﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("affine")]
    public class AffineController : Controller
    {
        // GET: Affine
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int a = 1, int shift = 1)
        {
            IEncryptor encryptor = new AffineEncryptor();
            return encryptor.encrypt(open_message, new object[] { a, shift });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int a = 1, int shift = 1)
        {
            IEncryptor encriptor = new AffineEncryptor();
            return encriptor.decrypt(enc_message, new object[] { a, shift });
        }
    }
}