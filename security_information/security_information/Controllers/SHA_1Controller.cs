﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models.Hasher;

namespace security_information.Controllers
{
    [RoutePrefix("sha-1")]
    public class SHA_1Controller : Controller
    {
        // GET: SHA_1
        public ActionResult Index()
        {
            return View();
        }

        [Route("hash")]
        [HttpPost]
        public String hash(String input = "sha")
        {
            IHasher hasher = new SHA_1Hasher();
            return hasher.hash(input);
        }
    }
}