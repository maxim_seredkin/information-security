﻿using System;

namespace security_information.Models
{
    /// <summary>
    ///     Класс, предоставляющий различные вспомогательные алгоритмы
    /// </summary>
    public class Algorithm
    {
        /// <summary>
        ///     Определениетого, что число является квадратичным вычетом по модулю
        /// </summary>
        /// <param name="a">Входное число</param>
        /// <param name="n">Модуль</param>
        /// <returns>Truem если число является квадратичным вычетом, иначе false</returns>
        public static bool IsQuadraticResidues(int a, int n)
        {
            return PowMod(a, (n - 1)/2, n) == 1;
        }


        /// <summary>
        ///     Возведение числа в степень по модулю
        /// </summary>
        /// <param name="a">Входящее число</param>
        /// <param name="k">Степень</param>
        /// <param name="n">Модуль</param>
        /// <returns>Число, возведенное в степень по модулю</returns>
        public static int PowMod(int a, int k, int n)
        {
            var b = 1;

            while (k > 0)
                if (k%2 == 0)
                {
                    k /= 2;
                    a = a*a%n;
                }
                else
                {
                    k--;
                    b = b*a%n;
                }

            return b;
        }

        /// <summary>
        ///     Проверка числа на простоту
        /// </summary>
        /// <param name="x">Входящее число x</param>
        /// <returns>True, если число просто, иначе false</returns>
        public static bool IsSimple(int x)
        {
            for (var i = 2; i < (int) Math.Sqrt(x); ++i)
                if (x%i == 0)
                    return false;
            return true;
        }

        /// <summary>
        ///     Проверка числа на простоту
        /// </summary>
        /// <param name="x">Входящее число x</param>
        /// <returns>True, если число просто, иначе false</returns>
        public static bool IsSimple(long x)
        {
            for (var i = 2L; i < (int)Math.Sqrt(x); ++i)
                if (x % i == 0L)
                    return false;
            return true;
        }

        /// <summary>
        ///     Нахождение взаимнообратного числа по модулю
        /// </summary>
        /// <param name="a">Входное число</param>
        /// <param name="n">Модуль</param>
        /// <returns>Обратное число по модулю</returns>
        public static int ReverseNum(int a, int n)
        {
            int x = 0, y = 0;
            gcd(a, n, ref x, ref y);
            return (x%n + n)%n;
        }

        /// <summary>
        ///     Расширенный алгоритм Евклида (ax + by = gcd(a,b))
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <param name="x">Коэффициент при a</param>
        /// <param name="y">Коэффициент при b</param>
        /// <returns>НОД</returns>
        public static int gcd(int a, int b, ref int x, ref int y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            int x1 = 0, y1 = 0;
            var d = gcd(b%a, a, ref x1, ref y1);
            x = y1 - b/a*x1;
            y = x1;
            return d;
        }

        /// <summary>
        ///     Алгоритм Евклида нахождения НОД
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <returns>НОД</returns>
        public static int gcd(int a, int b)
        {
            return b != 0 ? gcd(b, a%b) : a;
        }

        /// <summary>
        ///     Наименьшее общее кратное
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <returns>НОК</returns>
        public static int hcf(int a, int b)
        {
            return a*b/gcd(a, b);
        }

        /// <summary>
        ///     Прямое преобразование Гаусса
        /// </summary>
        /// <param name="matrix"></param>
        private static void gause(ref double[,] matrix)
        {
            for (uint k = 0; k < matrix.GetLength(0); ++k)
            {
                var temp = matrix[k, k];
                for (var i = k + 1; i < matrix.GetLength(0); ++i)
                {
                    var temp2 = matrix[i, k];
                    for (uint j = 0; j < matrix.GetLength(1); ++j)
                        matrix[i, j] -= matrix[k, j]*temp2/temp;
                }
            }
        }

        /// <summary>
        ///     Метод нахождения определителя
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Определитель матрицы</returns>
        public static double MatrixDetermined(double[,] matrix)
        {
            gause(ref matrix);
            double deter = 1;
            for (uint i = 0; i < matrix.GetLength(0); ++i)
                deter *= matrix[i, i];
            return Math.Round(deter);
        }

        /// <summary>
        ///     Метод умножения матриц по модулю
        /// </summary>
        /// <param name="matrix_a">Первая матрица</param>
        /// <param name="matrix_b">Вторая матрица</param>
        /// <param name="mod">Модуль</param>
        /// <returns>Умножение матриц</returns>
        public static double[,] MultiplyMatrix(double[,] matrix_a, double[,] matrix_b, int mod)
        {
            if (matrix_a.GetLength(1) != matrix_b.GetLength(0))
                return null;
            var matrix = new double[matrix_a.GetLength(0), matrix_b.GetLength(1)];
            for (var i = 0; i < matrix_a.GetLength(0); ++i)
                for (var j = 0; j < matrix_b.GetLength(1); ++j)
                {
                    for (var k = 0; k < matrix_b.GetLength(0); ++k)
                        matrix[i, j] += matrix_a[i, k]*matrix_b[k, j];
                    if ((matrix[i, j] %= mod) < 0)
                        matrix[i, j] += mod;
                }
            return matrix;
        }

        /// <summary>
        ///     Нахождение обратной матрицы по методу Гаусса
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Обратная матрица</returns>
        public static double[,] ReverseMatrixGause(double[,] matrix)
        {
            var matrix_one = new double[matrix.GetLength(0), matrix.GetLength(1)];

            for (var i = 0; i < matrix_one.GetLength(0); ++i)
                for (var j = 0; j < matrix_one.GetLength(1); ++j)
                    matrix_one[i, j] = i == j ? 1 : 0;

            for (uint k = 0; k < matrix.GetLength(0); ++k)
            {
                var temp = matrix[k, k];

                for (var i = k + 1; i < matrix.GetLength(0); ++i)
                {
                    var var_hcf = hcf((int) matrix[i, k], (int) matrix[k, k]);
                    int var_a = var_hcf/(int) matrix[i, k], var_b = var_hcf/(int) temp;

                    for (uint j = 0; j < matrix.GetLength(1); ++j)
                    {
                        matrix[i, j] = matrix[i, j]*var_a - matrix[k, j]*var_b;
                        matrix_one[i, j] = matrix_one[i, j]*var_a - matrix_one[k, j]*var_b;
                    }
                }
            }

            for (var k = matrix.GetLength(0) - 1; k > -1; --k)
            {
                var temp = matrix[k, k];

                for (var i = k - 1; i > -1; --i)
                {
                    var var_hcf = hcf((int) matrix[i, k], (int) matrix[k, k]);
                    int var_a = var_hcf/(int) matrix[i, k], var_b = var_hcf/(int) temp;

                    for (var j = matrix.GetLength(1) - 1; j > -1; --j)
                    {
                        matrix[i, j] = matrix[i, j]*var_a - matrix[k, j]*var_b;
                        matrix_one[i, j] = matrix_one[i, j]*var_a - matrix_one[k, j]*var_b;
                    }
                }
            }

            for (uint i = 0; i < matrix.GetLength(0); ++i)
            {
                for (uint j = 0; j < matrix.GetLength(1); ++j)
                {
                    matrix_one[i, j] = matrix_one[i, j]/matrix[i, i];
                }
            }

            return matrix_one;
        }

        /// <summary>
        ///     Нахождение обратной матрицы по модулю
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <param name="mod">Модуль</param>
        /// <returns>Обратная матрица по модулю</returns>
        public static double[,] ReverseMatrix(double[,] matrix, int mod)
        {
            matrix = MatrixTransform(matrix);

            var sol = new double[matrix.GetLength(0), matrix.GetLength(1)];

            int x = 0, y = 0;

            var r_det = gcd((int) MatrixDetermined((double[,]) matrix.Clone()), mod, ref x, ref y);

            r_det = (x%mod + mod)%mod;

            for (var k = 0; k < matrix.Length; ++k)
            {
                var tmp = new double[matrix.GetLength(0) - 1, matrix.GetLength(1) - 1];

                int row_a = k/matrix.GetLength(1), col_a = k%matrix.GetLength(1);

                for (int i = 0, j = 0; i < matrix.Length; ++i)
                {
                    int row = i/matrix.GetLength(1), col = i%matrix.GetLength(1);
                    if (!((row == row_a) || (col == col_a)))
                    {
                        tmp[j/tmp.GetLength(1), j%tmp.GetLength(1)] = matrix[row, col];
                        j++;
                    }
                }

                var temp = MatrixDetermined(tmp)*Math.Pow(-1, col_a + row_a)*r_det%mod;
                sol[row_a, col_a] = temp < 0 ? temp + mod : temp;
            }

            return sol;
        }

        /// <summary>
        ///     Транспонирование матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <returns>Транспонированная матрица</returns>
        public static double[,] MatrixTransform(double[,] matrix)
        {
            var sol = new double[matrix.GetLength(0), matrix.GetLength(1)];
            for (var i = 0; i < sol.GetLength(0); ++i)
                for (var j = 0; j < sol.GetLength(1); ++j)
                    sol[j, i] = (int) Math.Round(matrix[i, j]);
            return sol;
        }

        /// <summary>
        ///     Нахождение остатков для элементов матрицы
        /// </summary>
        /// <param name="matrix">Матрица</param>
        /// <param name="mod">Модуль</param>
        /// <returns>Матрица с остатками от деления</returns>
        public static double[,] MatrixMod(double[,] matrix, int mod)
        {
            var sol = new double[matrix.GetLength(0), matrix.GetLength(1)];
            for (var i = 0; i < sol.GetLength(0); ++i)
                for (var j = 0; j < sol.GetLength(1); ++j)
                    sol[i, j] = (int) Math.Round(matrix[i, j])%mod;
            return sol;
        }

        public static double[,] ParseStringToMatrix(string matrix)
        {
            var temp = matrix.Split('\n', ' ');

            var row = (int) Math.Sqrt(temp.Length);

            var return_matrix = new double[row, row];

            for (var i = 0; i < row; ++i)
                for (var j = 0; j < row; ++j)
                    return_matrix[i, j] = Convert.ToDouble(temp[i*row + j]);

            return return_matrix;
        }

        public static void swap(ref string a, ref string b)
        {
            var c = a;
            a = b;
            b = c;
            c = null;
        }
    }
}