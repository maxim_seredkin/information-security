﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("hill")]
    public class HillController : Controller
    {
        // GET: Hill
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int m = 1, String matrix = "")
        {

            IEncryptor encryptor = new HillEncryptor();
            return encryptor.encrypt(open_message, new object[] { m, matrix });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int m = 1, String matrix = "")
        {
            IEncryptor encriptor = new HillEncryptor();
            return encriptor.decrypt(enc_message, new object[] { m, matrix });
        }
    }
}