﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models.PRNG;

namespace security_information.Controllers
{
    [RoutePrefix("prng")]
    public class PRNGController : Controller
    {
        // GET: PRNG
        [Route("index")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: BBS
        [Route("bbs")]
        public ActionResult BBS(long p, long q, int l)
        {
            IPRNG prng = new BBS();
            return Json(prng.Next(p, q, l), JsonRequestBehavior.AllowGet);
        }
    }
}