﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Numerics;

namespace security_information.Models
{
    public class BigAlgorithm
    {

        /// <summary>
        /// Расширенный алгоритм Евклида (ax + by = gcd(a,b))
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <param name="x">Коэффициент при a</param>
        /// <param name="y">Коэффициент при b</param>
        /// <returns>НОД</returns>
        public static BigInteger gcd(BigInteger a, BigInteger b, ref BigInteger x, ref BigInteger y)
        {
            if (a == 0)
            {
                x = 0; y = 1;
                return b;
            }
            BigInteger x1 = 0, y1 = 0;
            BigInteger d = gcd(b % a, a, ref x1, ref y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }

        /// <summary>
        /// Алгоритм Евклида нахождения НОД
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <returns>НОД</returns>
        public static BigInteger gcd(BigInteger a, BigInteger b)
        {
            return ((b != 0) ? gcd(b, a % b) : a);
        }

        /// <summary>
        /// Наименьшее общее кратное
        /// </summary>
        /// <param name="a">Первое число</param>
        /// <param name="b">Второе число</param>
        /// <returns>НОК</returns>
        public static BigInteger hcf(BigInteger a, BigInteger b)
        {
            return a * b / gcd(a, b);
        }

    }
}