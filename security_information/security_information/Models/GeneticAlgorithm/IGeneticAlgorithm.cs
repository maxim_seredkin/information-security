﻿namespace security_information.Models.GeneticAlgorithm
{
    public interface IGeneticAlgorithm
    {
        GeneticAlgorithm.Evo Evolution(int countEpoches);
    }
}