﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace security_information.Models
{
    /// <summary>
    /// Интерфейс "Шифратор"
    /// </summary>
    interface IEncryptor
    {
        /// <summary>
        /// Зашифровать сообщение
        /// </summary>
        /// <param name="open_message">Открытое сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Зашифрованное сообщение</returns>
        String encrypt(String open_message, object[] keys);

        /// <summary>
        /// Расшифровать сообщение
        /// </summary>
        /// <param name="enc_message">Зашифрованное сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Открытое сообщение</returns>
        String decrypt(String enc_message, object[] keys);
    }
}
