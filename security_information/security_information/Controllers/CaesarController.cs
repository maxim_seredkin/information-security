﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("caesar")]
    public class CaesarController : Controller
    {
        // GET: Caesar
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int shift = 1)
        {
            IEncryptor encryptor = new CaesarEncryptor();
            return encryptor.encrypt(open_message,new object[] { shift });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int shift = 1)
        {
            IEncryptor encriptor = new CaesarEncryptor();
            return encriptor.decrypt(enc_message, new object[] { shift});
        }
    }
}