﻿namespace security_information.Models.DSA
{
    /// <summary>
    ///     Интерфейс для электронной цифровой подписи
    /// </summary>
    internal interface IDSA
    {
        // Создание ЭЦП
        object DSA(int hash, int a, int b, int n);

        // Верификация ЭЦП
        object VerificationDSA(int hash, int a, int b, int n, int q, int px, int py, int qx, int qy, int r, int s);
    }
}