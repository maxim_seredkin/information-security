﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace security_information.Models
{
    /// <summary>
    /// Шифратор "Шифр табличной маршрутной перестановки"
    /// </summary>
    public class SimplePermutationEncryptor : IEncryptor
    {
        /// <summary>
        /// Зашифровать сообщение
        /// </summary>
        /// <param name="open_message">Открытое сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Зашифрованное сообщение</returns>
        public String encrypt(String open_message, object[] keys)
        {
            if (open_message.Length == 0)
                return "-1|Неверный формат открытого сообщения";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("0|");
            int tmp = (int)Math.Ceiling((double)open_message.Length / (double)keys[0]);
            for (int i = 0; i < Convert.ToInt32(keys[0]); ++i)
                for (int j = 0; j < tmp; j++)
                {
                    if (i + j * Convert.ToInt32(keys[0]) < open_message.Length)
                        stringBuilder.Append(open_message[i + j * Convert.ToInt32(keys[0])]);
                    else
                        stringBuilder.Append('_');
                    if (((i * tmp + j + 1) % 5 == 0) && ((i * tmp + j + 1) != tmp* Convert.ToInt32(keys[0])))
                        stringBuilder.Append(' ');
                }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Расшифровать сообщение
        /// </summary>
        /// <param name="enc_message">Зашифрованное сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Открытое сообщение</returns>
        public String decrypt(String enc_message, object[] keys)
        {
            if (enc_message.Length == 0)
                return "-1|Неверный формат шифрограммы";
            StringBuilder stringBuilder = new StringBuilder();
            int tmp = (int)Math.Ceiling((double)enc_message.Length / 6.0);
            for (int i = 0; i < tmp - 1; ++i)
                stringBuilder.Append(enc_message.Substring(i * 6, 5));            
            stringBuilder.Append(enc_message.Substring((tmp - 1) * 6));
            enc_message = stringBuilder.ToString();
            if (enc_message.Length % Convert.ToInt32(keys[0]) != 0)
                return "-1|Неверная длина сообщения для введенного количества строк.";
            stringBuilder.Clear();
            stringBuilder.Append("0|");
            tmp = (int)Math.Ceiling((double)enc_message.Length / (double)Convert.ToInt32(keys[0]));
            for (int i = 0; i < tmp; ++i)
                for (int j = 0; j < Convert.ToInt32(keys[0]); j++)
                    stringBuilder.Append(enc_message[i + j * tmp]);            

            return stringBuilder.ToString().TrimEnd('_');
        }
    }
}