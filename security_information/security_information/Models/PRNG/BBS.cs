﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Web;
using security_information.Models.DSA;

namespace security_information.Models.PRNG
{
    /// <summary>
    /// ГПСЧ "Блюм-Блюм-Шуба"
    /// </summary>
    public class BBS : IPRNG
    {
        /// <summary>
        /// Получение следующего бита
        /// </summary>
        /// <param name="p">Просто число p</param>
        /// <param name="q">Просто число q</param>
        /// <param name="l">Количество бит последовательности</param>
        /// <returns>Массив битов последовательности</returns>
        public object Next(long p, long q, int l)
        {
            Random rand = new Random();

            ECDSA.DTO<object> dto = new ECDSA.DTO<object>();

            if ((!Algorithm.IsSimple(p)) || (!Algorithm.IsSimple(q)))
            {
                dto.Error = "p или q не простое число!";
                return dto;
            }


            if (((p % 4) == 3) && ((q % 4) == 3))
            {

                long n = p * q, _s = -1;

                while (true)
                {
                    _s = rand.Next(1, (int)n);
                    if (BigInteger.GreatestCommonDivisor(_s, n) == 1)
                        break;
                }

                List<BigInteger> s = new List<BigInteger>();
                List<int> z = new List<int>();

                s.Add(BigInteger.ModPow(_s, 2, n));

                for (int i = 0; i < l; ++i)
                {
                    s.Add(BigInteger.ModPow(s[i], 2, n));
                    z.Add((int)(s.Last() % 2));
                }

                dto.Response = z;
                return dto;

            }

            dto.Error = "p и q введены неправильно!";
            return dto;
        }
    }
}