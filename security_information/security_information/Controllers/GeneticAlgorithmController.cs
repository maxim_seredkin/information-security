﻿using System.Web.Mvc;
using security_information.Models.GeneticAlgorithm;

namespace security_information.Controllers
{
    public class GeneticAlgorithmController : Controller
    {
        // GET: GeneticAlgorithm
        public ActionResult Index(int countEpoches = 10)
        {
            IGeneticAlgorithm genetic = new GeneticAlgorithm();
            return Json(genetic.Evolution(countEpoches), JsonRequestBehavior.AllowGet);
        }
    }
}