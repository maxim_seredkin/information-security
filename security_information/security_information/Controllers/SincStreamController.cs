﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("sincstream")]
    public class SincStreamController : Controller
    {
        // GET: SincStream
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int k = 1)
        {
            IEncryptor encryptor = new SincStreamEncryptor();
            return encryptor.encrypt(open_message, new object[] { k });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int k = 1)
        {
            IEncryptor encriptor = new SincStreamEncryptor();
            return encriptor.decrypt(enc_message, new object[] { k });
        }
    }
}