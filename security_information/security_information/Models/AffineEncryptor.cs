﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace security_information.Models
{
    /// <summary>
    /// Шифратор "Аффинный шифр"
    /// </summary>
    public class AffineEncryptor : IEncryptor
    {
        //List<char> alp = new List<char>(new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }.ToList<Char>());

        /// <summary>
        /// Используемый алфавит
        /// </summary>
        List<char> alp = new List<char>(new[] { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' }.ToList<Char>());

        /// <summary>
        /// Зашифровать сообщение
        /// </summary>
        /// <param name="open_message">Открытое сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Зашифрованное сообщение</returns>
        public String encrypt(String open_message, object[] keys)
        {
            if (Algorithm.gcd(Convert.ToInt32(keys[0]), alp.Count) != 1)
                return "-1|Число a не взаимно простое с количеством символов алфавита.";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("0|");
            for (int i = 0; i < open_message.Length; ++i)
                if (open_message[i] != ' ')
                    stringBuilder.Append(alp[(Convert.ToInt32(keys[0]) * alp.FindIndex(it => it == open_message[i]) + Convert.ToInt32(keys[1])) % alp.Count]);
                else
                    stringBuilder.Append(' ');

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Расшифровать сообщение
        /// </summary>
        /// <param name="enc_message">Зашифрованное сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Открытое сообщение</returns>
        public String decrypt(String enc_message, object[] keys)
        {
            int _a = 0, x = 0, y = 0;
            if (Algorithm.gcd(Convert.ToInt32(keys[0]), alp.Count, ref x, ref y) != 1)
                return "-1|Число a не взаимно простое с количеством символов алфавита.";
            _a = (x % alp.Count + alp.Count) % alp.Count;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("0|");
            for (int i = 0; i < enc_message.Length; ++i) {
                if (enc_message[i] != ' ') {
                    int tmp = (_a * (alp.FindIndex(it => it == enc_message[i]) - Convert.ToInt32(keys[1]))) % alp.Count;
                    tmp = (tmp < 0 ? tmp + alp.Count : tmp);
                    stringBuilder.Append(alp[tmp]);
                }
                else
                    stringBuilder.Append(' ');
            }

            return stringBuilder.ToString();
        }
    }
}