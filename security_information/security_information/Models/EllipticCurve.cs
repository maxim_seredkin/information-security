﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace security_information.Models
{
    /// <summary>
    ///     Класс эллиптическая кривая
    /// </summary>
    internal class EllipticCurve
    {
        public int range = -1;
        public int index = -1;
        public int A { get; }
        public int B { get; }
        public int N { get; }
        public Point StartPoint { get; private set; }

        public List<List<Point>> Points { get; set; }


        private EllipticCurve(int a, int b, int n)
        {
            A = a;
            B = b;
            N = n;
        }

        public bool CreateStartPoint(int x)
        {
            var y2 = (int)(Math.Pow(x, 3) + A * x + B) % N;

            if (Algorithm.IsQuadraticResidues(y2, N))
                for (var i = 0; i < N; ++i)
                    if (i * i % N == y2)
                    {
                        StartPoint = new Point(x, i, A, B, N);
                        return true;
                    }

            return false;
        }

        /// <summary>
        ///     Вычисление ранга группы точек
        /// </summary>
        /// <returns></returns>
        public int RangeEC()
        {
            if (range == -1)
            {
                range = 1;
                var pt = StartPoint;
                for (var i = 1; ; ++i)
                    if (i == 1)
                    {
                        pt = Point.Double(pt);
                        ++range;
                    }
                    else if (StartPoint.X != pt.X)
                    {
                        pt = pt + StartPoint;
                        ++range;
                    }
                    else
                        break;
                ++range;
            }
            return range;
        }

        public int RangeECWithInclude()
        {
            if (Points == null)
            {
                Points = new List<List<Point>>();
                for (int x = 0; x < N; ++x)
                {
                    var y2 = (int)(Math.Pow(x, 3) + A * x + B) % N;
                    if (Algorithm.IsQuadraticResidues(y2, N))
                    {
                        int y = Algorithm.PowMod(y2, (N + 1) / 4, N);
                        Points.Add(new List<Point>());
                        Points.Last().Add(new Point(x, y, A, B, N));
                        for (var j = 1; ; ++j)
                            if (j == 1)
                                Points.Last().Add(Point.Double(Points.Last()[j - 1]));
                            else if (Points.Last().First().X != Points.Last()[j - 1].X)
                                Points.Last().Add(Points.Last()[j - 1] + Points.Last().First());
                            else
                            {
                                if (range < Points.Last().Count + 1)
                                {
                                    range = Points.Last().Count + 1;
                                    StartPoint = Points.Last().First();
                                    index = Points.IndexOf(Points.Last());
                                }
                                break;
                            }
                        Points.Add(new List<Point>());
                        Points.Last().Add(new Point(x, N - y, A, B, N));
                        for (var j = 1; ; ++j)
                            if (j == 1)
                                Points.Last().Add(Point.Double(Points.Last()[j - 1]));
                            else if (Points.Last().First().X != Points.Last()[j - 1].X)
                                Points.Last().Add(Points.Last()[j - 1] + Points.Last().First());
                            else
                            {
                                if (range < Points.Last().Count + 1)
                                {
                                    range = Points.Last().Count + 1;
                                    StartPoint = Points.Last().First();
                                    index = Points.IndexOf(Points.Last());
                                }
                                break;
                            }
                    }
                }
            }

            return range;
        }

        public Point Multiply(int x)
        {
            return x * StartPoint;
        }

        public static class Builder
        {
            public static EllipticCurve Build(int a, int b, int n)
            {
                return (int)(4 * Math.Pow(a, 3) + 27 * Math.Pow(b, 2)) % n != 0 ? new EllipticCurve(a, b, n) : null;
            }
        }

        /// <summary>
        ///     Класс "Точка эллпитической кривой"
        /// </summary>
        public class Point
        {
            public Point()
            {
            }

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public Point(int x, int y, int a, int b, int n)
            {
                X = x;
                Y = y;
                A = a;
                B = b;
                N = n;
            }

            public int X { get; set; }
            public int Y { get; set; }
            private int A { get; set; }
            private int B { get; set; }
            private int N { get; set; }

            /// <summary>
            ///     Оператор сложения двух точек
            /// </summary>
            /// <param name="p1"></param>
            /// <param name="p2"></param>
            /// <returns></returns>
            public static Point operator +(Point p1, Point p2)
            {
                var nP = new Point();
                nP.A = p1.A;
                nP.B = p1.B;
                nP.N = p1.N;

                var dy = p2.Y - p1.Y;
                var dx = p2.X - p1.X;

                if (dx < 0)
                    dx += p1.N;
                if (dy < 0)
                    dy += p1.N;

                var m = dy * Algorithm.ReverseNum(dx, p1.N) % p1.N;
                if (m < 0)
                    m += p1.N;
                nP.X = (m * m - p1.X - p2.X) % p1.N;
                nP.Y = (m * (p1.X - nP.X) - p1.Y) % p1.N;
                if (nP.X < 0)
                    nP.X += p1.N;
                if (nP.Y < 0)
                    nP.Y += p1.N;
                return nP;
            }

            /// <summary>
            ///     Удвоение точки
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            public static Point Double(Point p)
            {
                var nP = new Point();
                nP.A = p.A;
                nP.B = p.B;
                nP.N = p.N;

                var dy = 3 * p.X * p.X + p.A;
                var dx = 2 * p.Y;

                if (dx < 0)
                    dx += p.N;
                if (dy < 0)
                    dy += p.N;

                var m = dy * Algorithm.ReverseNum(dx, p.N) % p.N;
                nP.X = (m * m - p.X - p.X) % p.N;
                nP.Y = (m * (p.X - nP.X) - p.Y) % p.N;
                if (nP.X < 0)
                    nP.X += p.N;
                if (nP.Y < 0)
                    nP.Y += p.N;

                return nP;
            }

            /// <summary>
            ///     Оператор умножения точки на число
            /// </summary>
            /// <param name="p"></param>
            /// <param name="x"></param>
            /// <returns></returns>
            public static Point operator *(Point p, int x)
            {
                Point start = (Point)p.MemberwiseClone(), result = (Point)p.MemberwiseClone();

                for (var i = 2; i <= x; ++i)
                    if (start.X != result.X)
                        result = result + start;
                    else
                        result = Double(result);

                return result;
            }

            /// <summary>
            ///     Оператор умножение числа на точку
            /// </summary>
            /// <param name="x"></param>
            /// <param name="p"></param>
            /// <returns></returns>
            public static Point operator *(int x, Point p)
            {
                Point start = (Point)p.MemberwiseClone(), result = (Point)p.MemberwiseClone();

                for (var i = 2; i <= x; ++i)
                    if (start.X != result.X)
                        result = result + start;
                    else
                        result = Double(result);

                return result;
            }
        }
    }
}