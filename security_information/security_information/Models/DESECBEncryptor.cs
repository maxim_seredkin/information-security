﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace security_information.Models
{
    /// <summary>
    /// Шифратор "DES-ECB"
    /// </summary>
    class DESECBEncryptor : IEncryptor
    {
        Byte[] shift = new Byte[] { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 },
       PC1 = new Byte[] { 57, 49, 41, 33, 25, 17,  9,
           1, 58, 50, 42, 34, 26, 18,
          10,  2, 59, 51, 43, 35, 27,
          19, 11,  3, 60, 52, 44, 36,
          63, 55, 47, 39, 31, 23, 15,
           7, 62, 54, 46, 38, 30, 22,
          14,  6, 61, 53, 45, 37, 29,
          21, 13,  5, 28, 20, 12,  4 },
           PC2 = new Byte[] { 14, 17, 11, 24,  1,  5,
               3, 28, 15,  6, 21, 10,
              23, 19, 12,  4, 26,  8,
              16,  7, 27, 20, 13,  2,
              41, 52, 31, 37, 47, 55,
              30, 40, 51, 45, 33, 48,
              44, 49, 39, 56, 34, 53,
              46, 42, 50, 36, 29, 32 },
           IP = new Byte[] { 58, 50, 42, 34, 26, 18, 10,  2,
             60, 52, 44, 36, 28, 20, 12,  4,
             62, 54, 46, 38, 30, 22, 14,  6,
             64, 56, 48, 40, 32, 24, 16,  8,
             57, 49, 41, 33, 25, 17,  9,  1,
             59, 51, 43, 35, 27, 19, 11,  3,
             61, 53, 45, 37, 29, 21, 13,  5,
             63, 55, 47, 39, 31, 23, 15,  7 },
           P = new Byte[] { 16,  7, 20, 21, 29, 12, 28, 17,
             1, 15, 23, 26,  5, 18, 31, 10,
             2,  8, 24, 14, 32, 27,  3,  9,
            19, 13, 30,  6, 22, 11,  4, 25 };
        Byte PC1_s = 56,
            P_s = 32,
            IP_s = 64,
            PC2_s = 48;

        Byte[,] S = new Byte[8, 64] { {14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
                              0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
                              4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
                             15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13},
                            {15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
                              3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
                              0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
                             13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9},
                            {10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
                             13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
                             13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
                              1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12},
                            { 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
                             13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
                             10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
                              3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14},
                            { 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
                             14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
                              4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
                             11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3},
                            {12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
                             10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
                              9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
                              4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13},
                            { 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
                             13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
                              1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
                              6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12},
                            {13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
                              1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
                              7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
                              2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11} };

        /// <summary>
        /// Преобразование в 10-тичные числа
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Byte To_dec(String input)
        {
            return Convert.ToByte(input, 2); // (uint)bitset < 8 > (input).to_ulong();
        }

        /// <summary>
        /// Сложение по модулю 2
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        String Sum_mod_2(String a, String b)
        {
            String res = "";
            for (int i = 0; i < a.Length; ++i)
                res += ((((a[i] == '1') && (b[i] == '1') || (a[i] == '0') && (b[i] == '0'))) ? "0" : "1");
            return res;
        }


        /// <summary>
        /// Преобразование в тип String
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        String ToString(String input)
        {
            String res = "";
            for (int i = 0; i < input.Length / 8; ++i)
                res += (char)Convert.ToByte(input.Substring(i * 8, 8),2);
            return res;
        }

        /// <summary>
        /// Преобразование в 16-ричное представление
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        String ToHEX(String input)
        {
            String res = "";
            for (int i = 0; i < input.Length / 8; ++i)
                res += Convert.ToString(Convert.ToByte(input.Substring(i * 8, 8), 2), 16); // bitset < 8 > (input.Substring(i * 8, 8)).to_ulong();
            return res;
        }

        /// <summary>
        /// Циклический сдвиг влево
        /// </summary>
        /// <param name="input"></param>
        /// <param name="sh"></param>
        void Cycle_shift(ref String input, int sh)
        {
            input = input.Substring(sh, input.Length - sh) + input.Substring(0, sh);
        }

        /// <summary>
        /// Преобразование в биты
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        String To_bits(String input)
        {
            String res = "";
            for (int i = 0; i < input.Length; ++i)
                res += Convert.ToString(Convert.ToByte(input[i]), 2).PadLeft(8, '0'); //  bitset < 8 > ((uint)input[i]).ToString();
            return res;
        }

        /// <summary>
        /// Перестановки
        /// </summary>
        /// <param name="input"></param>
        /// <param name="permutation"></param>
        /// <param name="permutation_s"></param>
        /// <param name="_ip"></param>
        /// <returns></returns>
        String Per(String input, Byte[] permutation, Byte permutation_s, bool _ip)
        {

            StringBuilder output = new StringBuilder();
            if (_ip)
                output.Length = 64;// new StringBuilder(64)
            for (int i = 0; i < permutation_s; ++i)
                if (_ip)
                    output[permutation[i] - 1] = input[i];
                else
                    output.Append(input[permutation[i] - 1]);
            return output.ToString();
        }

        /// <summary>
        /// Генерация ключей
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        List<String> Gen_key(String input)
        {
            List<String> res = new List<String>(16);
            String tmpkey = "", binput = To_bits(input);
            for (int i = 0; i < binput.Length / 7; ++i) // подсчет битов четности
            {
                String tmp = binput.Substring(i * 7, 7);
                tmpkey += ((tmp.Count(p => p == '1') % 2) == 1 ? tmp + "0" : tmp + "1");
            }
            tmpkey = Per(tmpkey, PC1, PC1_s, false); // перестановка PC1
            String C = tmpkey.Substring(0, 28), D = tmpkey.Substring(28, 28);
            for (int i = 0; i < 16; ++i)
            {
                Cycle_shift(ref C, shift[i]);
                Cycle_shift(ref D, shift[i]);
                res.Add(Per((C + D), PC2, PC2_s, false)); // перестановка PC2
            }
            return res;
        }

        /// <summary>
        /// Замена для шифрования
        /// </summary>
        /// <param name="h"></param>
        /// <param name="it"></param>
        /// <returns></returns>
        String Get_t(String h, int it)
        {
            return Convert.ToString(S[it, To_dec(h.Substring(0, 1) + h.Substring(5, 1)) * 16 + To_dec(h.Substring(1, 4))], 2).PadLeft(4, '0');
        }

        /// <summary>
        /// Функция f шифрарования
        /// </summary>
        /// <param name="L"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        String f(String L, String k)
        {
            String res = "", E = "", H = "";
            L = L.Substring(L.Length - 1, 1) + L + L.Substring(0, 1); // расширение E
            for (int i = 0; i < L.Length - 5; i += 4)
                E += L.Substring(i, 6);
            H = Sum_mod_2(E, k); // сложение по модулю 2 с ключом
            for (int i = 0; i < H.Length / 6; ++i) // преобразование s->t
                res += Get_t(H.Substring(i * 6, 6), i);
            res = Per(res, P, P_s, false); // перестановка P
            return res;
        }

        /// <summary>
        /// Раунды DES-ECB
        /// </summary>
        /// <param name="input"></param>
        /// <param name="bkey"></param>
        /// <param name="decript"></param>
        /// <returns></returns>
        String Rounds(String input, List<String> bkey, bool decript)
        {
            String H = input.Substring(0, 32), L = input.Substring(32, 32);
            for (int i = 0; i < 16; ++i)
            {
                H = ((decript) ? Sum_mod_2(H, f(L, bkey[15 - i])) : Sum_mod_2(H, f(L, bkey[i])));
                if (i != 15) Algorithm.swap(ref L, ref H); // не менять в последнем раунде
            }
            return (H + L);
        }

        /// <summary>
        /// Шифравоение DES
        /// </summary>
        /// <param name="input_message"></param>
        /// <param name="input_key"></param>
        /// <param name="decrypt"></param>
        /// <returns></returns>
        String DES(String input_message, String input_key, bool decrypt)
        {

            input_message = input_message.PadRight( (int) Math.Ceiling((double)input_message.Length / 8.0)*8,'x');
            input_key = (input_key.Length >= 7) ? input_key.Substring(0,7):input_key.PadRight(7,'x');

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < input_message.Length / 8; ++i)
            {
                string temp = Per(Rounds(Per(To_bits(input_message.Substring(i * 8, 8)), IP, IP_s, false), Gen_key(input_key), decrypt), IP, IP_s, true);
                result.Append(temp);
            }

            return result.ToString();
        }

        /// <summary>
        /// Зашифровать сообщение
        /// </summary>
        /// <param name="open_message">Открытое сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Зашифрованное сообщение</returns>
        public String encrypt(String open_message, object[] keys)
        {
            string result = DES(open_message, (String)keys[0], false);
            return "\"" + ToString(result) + "\", \"" + ToHEX(result) + "\", \"" + result + "\"";
        }

        /// <summary>
        /// Расшифровать сообщение
        /// </summary>
        /// <param name="enc_message">Зашифрованное сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Открытое сообщение</returns>
        public String decrypt(String enc_message, object[] keys)
        {
            return "\"" + ToString(DES(enc_message, (String)keys[0], true)) + "\"";
        }

    }
}
