﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace security_information.Models.PRNG
{
    interface IPRNG
    {
        object Next(long p, long q, int l);
    }
}
