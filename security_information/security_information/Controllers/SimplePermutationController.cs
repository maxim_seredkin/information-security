﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("simplepermutation")]
    public class SimplePermutationController : Controller
    {
        // GET: SimplePermutation
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int count_rows = 1)
        {
            IEncryptor encryptor = new SimplePermutationEncryptor();
            return encryptor.encrypt(open_message, new object[] { count_rows });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int count_rows = 1)
        {
            IEncryptor encriptor = new SimplePermutationEncryptor();
            return encriptor.decrypt(enc_message, new object[] { count_rows });
        }
    }
}