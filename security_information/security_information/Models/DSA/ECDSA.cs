﻿using System;

namespace security_information.Models.DSA
{
    /// <summary>
    ///     Электронная цифровая подпись на базе эллиптических кривых
    /// </summary>
    internal class ECDSA : IDSA
    {
        private OpenKey _openKey;
        private int PrivateKey { get; set; }

        // Создание ЭЦП
        public object DSA(int hash, int a, int b, int n)
        {
            var dto = new DTO<object[]>();

            dto.Error = GenerateOpenKey(a, b, n);
            if (dto.Error != null)
                return dto;

            dto.Response = new object[] {_openKey, GenerateDsa(hash)};

            return dto;
        }

        // Верификация ЭЦП
        public object VerificationDSA(int hash, int a, int b, int n, int q, int px, int py, int qx, int qy, int r, int s)
        {
            _openKey.A = a;
            _openKey.B = b;
            _openKey.N = n;
            _openKey.Q = q;
            _openKey.PPoint = new EllipticCurve.Point(px, py, a, b, n);
            _openKey.QPoint = new EllipticCurve.Point(qx, qy, a, b, n);

            var _e = hash%_openKey.Q;
            var v = Algorithm.ReverseNum(_e, _openKey.Q);
            int z1 = s*v%_openKey.Q, z2 = (_openKey.Q - r)*v%_openKey.Q;
            var c = z1*_openKey.PPoint + z2*_openKey.QPoint;
            var _r = c.X%_openKey.Q;

            return _r == r ? "Верификация прошла успешно!" : "Верификация провелена!";
        }

        // Генерирование ЭЦП
        private DsaStruct GenerateDsa(int hash)
        {
            var rand = new Random();
            var result = new DsaStruct();

            for (var i = 0;; ++i)
            {
                var e = hash%_openKey.Q;
                var k = rand.Next(1, _openKey.Q - 1);
                var c = k*_openKey.PPoint;
                var r = c.X%_openKey.Q;

                if (r == 0)
                    continue;

                var s = (r*PrivateKey + k*e)%_openKey.Q;

                if (s == 0)
                    continue;

                result.R = r;
                result.S = s;

                break;
            }

            return result;
        }

        // Генерирование открытого ключа
        private string GenerateOpenKey(int a, int b, int n)
        {
            if (!Algorithm.IsSimple(n))
                return "Модуль не является простым числом";

            var rand = new Random();
            EllipticCurve.Point qPoint = null;

            var curve = EllipticCurve.Builder.Build(a, b, n);
            if (curve == null)
                return "Необходимо выбрать другие коэффициенты A и B!";

            for (var counter = 0;; ++counter)
            {
                var flag = false;
                if (counter == 100)
                    return "Попробуйте ввести другие коэффициенты A и B";
                var xp = (int) (rand.NextDouble()*n);
                if ((xp <= 0) || (n <= xp))
                    continue;
                if (!curve.CreateStartPoint(xp))
                    continue;
                var q = curve.RangeEC();
                if (!Algorithm.IsSimple(q))
                    return "Попробуйте ввести другие коэффициенты A и B";
                var T = rand.Next(11, 33);
                for (var i = 1; i <= T; ++i)
                    if (Algorithm.PowMod(n, i, q) == 1)
                    {
                        flag = true;
                        break;
                    }
                if (flag) continue;

                PrivateKey = rand.Next(1, q);
                qPoint = curve.Multiply(PrivateKey);
                break;
            }

            _openKey.A = a;
            _openKey.B = b;
            _openKey.N = n;
            _openKey.PPoint = curve.StartPoint;
            _openKey.QPoint = qPoint;
            _openKey.Q = curve.RangeEC();

            return null;
        }

        /// <summary>
        ///     Объект, серилизующий при отправке в Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class DTO<T>
        {
            public string Error { get; set; }
            public T Response { get; set; }
        }

        // ЭЦП
        public struct DsaStruct
        {
            public int R { get; set; }
            public int S { get; set; }
        }

        // Открытый ключ
        public struct OpenKey
        {
            public int A { get; set; }
            public int B { get; set; }
            public int N { get; set; }
            public int Q { get; set; }
            public EllipticCurve.Point PPoint { get; set; }
            public EllipticCurve.Point QPoint { get; set; }
        }
    }
}