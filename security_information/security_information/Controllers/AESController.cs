﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using security_information.Models;

namespace security_information.Controllers
{
    [RoutePrefix("aes")]
    public class AESController : Controller
    {
        // GET: AES
        public ActionResult Index()
        {
            return View();
        }

        [Route("encryption")]
        [HttpPost]
        public String Encript(String open_message = "", int shift = 1)
        {
            IEncryptor encryptor = new AESEncryptor();
            return encryptor.encrypt(open_message, new object[] { shift });
        }

        [Route("decryption")]
        [HttpPost]
        public String Decript(String enc_message = "", int shift = 1)
        {
            IEncryptor encriptor = new AESEncryptor();
            return encriptor.decrypt(enc_message, new object[] { shift });
        }

        [Route("generate-keys")]
        [HttpPost]
        public ActionResult GenerateKeys(String key = "", int size = 128)
        {        
            AESEncryptor encryptor = new AESEncryptor();
            return Json(encryptor.GenerateKeys(key, new object[] { size }));
        }
    }
}