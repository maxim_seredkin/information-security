﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Text;


namespace security_information.Models
{
    /// <summary>
    /// Шифратор "Шифр Хилла"
    /// </summary>
    public class HillEncryptor : IEncryptor
    {
        /// <summary>
        /// Используемый алфавит
        /// </summary>
        List<char> alp = new List<char>(new[] { 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' }.ToList<Char>());

        /// <summary>
        /// Зашифровать сообщение
        /// </summary>
        /// <param name="open_message">Открытое сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Зашифрованное сообщение</returns>
        public string encrypt(string open_message, object[] keys)
        {

            StringBuilder sb = new StringBuilder();
            Random random = new Random();
            double[,] matrix = Algorithm.ParseStringToMatrix((string)keys[1]); //new double[Convert.ToInt32(keys[0]), Convert.ToInt32(keys[0])];
            open_message = open_message.PadRight(open_message.Length + ((open_message.Length % Convert.ToInt32(keys[0]) == 0)?0: Convert.ToInt32(keys[0]) - open_message.Length % Convert.ToInt32(keys[0])), 'я');

            //test:
            //for (int i = 0; i < Convert.ToInt32(keys[0]); ++i)
            //    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
            //        matrix[i, j] = random.Next(1, 33);
            
            if (Algorithm.MatrixDetermined((double[,])matrix.Clone()) > 0 && Algorithm.MatrixDetermined((double[,])matrix.Clone())%alp.Count != 0)
            {
                sb.Append("0|");

                for (int i = 0; i < open_message.Length / Convert.ToInt32(keys[0]); ++i)
                {
                    double[,] matrix_a = new double[1, Convert.ToInt32(keys[0])];
                    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
                        matrix_a[0, j] = alp.FindIndex(x => x == open_message[i * Convert.ToInt32(keys[0]) + j]);
                    double[,] tmp = Algorithm.MultiplyMatrix(matrix_a, matrix, alp.Count);
                    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
                        sb.Append(alp[(int)tmp[0, j]]);
                }

                return sb.ToString();
            }
            else
                return ("-1|Матрица не удовлетворяет условиям!");
                //goto test;
        }

        /// <summary>
        /// Расшифровать сообщение
        /// </summary>
        /// <param name="enc_message">Зашифрованное сообщение</param>
        /// <param name="keys">Ключи, используемые для шифрования</param>
        /// <returns>Открытое сообщение</returns>
        public string decrypt(string enc_message, object[] keys)
        {
            if (enc_message.Length % Convert.ToInt32(keys[0]) != 0)
                return "-1|Неправильный ключ или шифрограмма";

            StringBuilder sb = new StringBuilder();
            Random random = new Random();
            
            double[,] matrix = Algorithm.ParseStringToMatrix((string)keys[1]); //new double[Convert.ToInt32(keys[0]), Convert.ToInt32(keys[0])];

            //test:
            //for (int i = 0; i < Convert.ToInt32(keys[0]); ++i)
            //    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
            //        matrix[i, j] = random.Next(1, 33);

            if (Algorithm.MatrixDetermined((double[,])matrix.Clone()) > 0 && Algorithm.MatrixDetermined((double[,])matrix.Clone()) % alp.Count != 0)
            {
                sb.Append("0|");

                double[,] reverse_matrix = Algorithm.ReverseMatrix((double[,])matrix.Clone(), alp.Count);

                for (int i = 0; i < enc_message.Length / Convert.ToInt32(keys[0]); ++i)
                {
                    double[,] matrix_a = new double[1, Convert.ToInt32(keys[0])];
                    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
                        matrix_a[0, j] = alp.FindIndex(x => x == enc_message[i * Convert.ToInt32(keys[0]) + j]);
                    double[,] tmp = Algorithm.MultiplyMatrix(matrix_a, reverse_matrix, alp.Count);
                    for (int j = 0; j < Convert.ToInt32(keys[0]); ++j)
                        sb.Append(alp[(int)tmp[0, j]]);
                }

                return sb.ToString();
            }
            else
                return ("-1|Матрица не удовлетворяет условиям!");
                //goto test;
        }
    }
}