﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace security_information.Models.GeneticAlgorithm
{
    /// <summary>
    /// Генетический алгоритм
    /// </summary>
    public class GeneticAlgorithm : IGeneticAlgorithm
    {
       /// <summary>
       /// Реализация метода "Эволюционирования" интерфейса "IGeneticAlgorithm"
       /// </summary>
       /// <param name="countEpoches"></param>
       /// <returns></returns>
        public Evo Evolution(int countEpoches)
        {
            return new Evo(countEpoches);
        }

        /// <summary>
        /// Класс "Эволюция"
        /// </summary>
        public class Evo
        {
            public readonly List<Epoch> Epoches = new List<Epoch>();
            public readonly List<int> Properties = new List<int>();

            /// <summary>
            /// Конструктор класса
            /// </summary>
            /// <param name="countEpoches"></param>
            public Evo(int countEpoches)
            {
                var rand = new Random();
                for (var i = 0; i < 6; ++i)
                    Properties.Add(rand.Next(0, 15));

                for (var i = 0; i < countEpoches; ++i)
                    Epoches.Add(i == 0 ? new Epoch(Properties) : new Epoch(Epoches.Last()));
            }

            /// <summary>
            /// Класс "Эпоха", поколение
            /// </summary>
            public class Epoch
            {
                public readonly List<Children> Childrens = new List<Children>();
                public double maxSum = -1;

                /// <summary>
                /// Конструктор эпохи - начальная эпоха, зарождение первородных
                /// </summary>
                /// <param name="properties"></param>
                public Epoch(List<int> properties)
                {
                    var rand = new Random();
                    for (var i = 0; i < 12; ++i)
                    {
                        Childrens.Add(
                            new Children(
                                (properties[rand.Next(0, properties.Count - 1)] << 4) |
                                properties[rand.Next(0, properties.Count - 1)], 8));
                        if (maxSum < Childrens.Last().Sum)
                            maxSum = Childrens.Last().Sum;
                    }

                    Childrens.ForEach(children => children.Fitness = Fitness(children, maxSum));
                    Childrens.Sort(
                        (children, children1) =>
                            children.Fitness < children1.Fitness ? 1 : children.Fitness > children1.Fitness ? -1 : 0);
                }

                /// <summary>
                /// Конструктор для наследников
                /// </summary>
                /// <param name="epoch"></param>
                public Epoch(Epoch epoch)
                {
                    var rand = new Random();
                    double sumFitness = 0, randFitness = 0.0;
                    foreach (var ch in epoch.Childrens)
                        sumFitness += ch.Fitness;

                    var tempFit = new List<double>();
                    tempFit.Add(0.0);
                    epoch.Childrens.ForEach(x => tempFit.Add(tempFit.Last() + x.Fitness));
                    Children first = null, second = null;
                    for (var i = 0; i < 3; ++i)
                    {
                        randFitness = sumFitness*rand.NextDouble();
                        for (var j = 1; j < tempFit.Count; ++j)
                            if ((randFitness >= tempFit[j - 1]) && (randFitness <= tempFit[j]))
                            {
                                first = epoch.Childrens[j - 1];
                                break;
                            }
                        randFitness = sumFitness*rand.NextDouble();
                        for (var j = 1; j < tempFit.Count; ++j)
                            if ((randFitness >= tempFit[j - 1]) && (randFitness <= tempFit[j]))
                            {
                                second = epoch.Childrens[j - 1];
                                break;
                            }
                        Childrens.Add(first + second);
                        if (maxSum < Childrens.Last().Sum)
                            maxSum = Childrens.Last().Sum;
                        Childrens.Add(second + first);
                        if (maxSum < Childrens.Last().Sum)
                            maxSum = Childrens.Last().Sum;
                    }
                    
                    epoch.Childrens.ForEach(x =>
                    {
                        if (rand.NextDouble()*100 < 36)
                        {
                            Childrens.Add(x.Mutate());
                            if (maxSum < Childrens.Last().Sum)
                                maxSum = Childrens.Last().Sum;
                        }
                    });

                    Childrens.ForEach(children => children.Fitness = Fitness(children, maxSum));
                    Childrens.Sort(
                        (children, children1) =>
                            children.Fitness < children1.Fitness ? 1 : children.Fitness > children1.Fitness ? -1 : 0);
                }

                /// <summary>
                /// Фитнес-функция
                /// </summary>
                /// <param name="children"></param>
                /// <param name="maxSum"></param>
                /// <returns></returns>
                private double Fitness(Children children, double maxSum)
                {
                    return Convert.ToDouble(children.Sum)/maxSum;
                }

                /// <summary>
                /// Класс потомок
                /// </summary>
                public class Children
                {
                    /// <summary>
                    /// Конструктор класса
                    /// </summary>
                    /// <param name="value"></param>
                    /// <param name="length"></param>
                    public Children(int value, int length)
                    {
                        var sum = 0;
                        for (var tempValue = value; tempValue != 0; tempValue >>= 1)
                            sum += tempValue%2;
                        Value = value;
                        Length = length;
                        Sum = sum;
                    }

                    public int Value { get; }
                    public int Length { get; }
                    public int Sum { get; }
                    public double Fitness { get; set; }

                    /// <summary>
                    /// Перегрузка оператора - двойной кроссовер
                    /// </summary>
                    /// <param name="c1"></param>
                    /// <param name="c2"></param>
                    /// <returns></returns>
                    public static Children operator +(Children c1, Children c2)
                    {
                        var separator = 2;

                        return
                            new Children(
                                ((c1.Value >> separator) << separator) |
                                c2.Value & Convert.ToInt32(Math.Pow(2, separator) - 1),
                                c1.Length);
                    }

                    /// <summary>
                    /// Функция мутация
                    /// </summary>
                    /// <returns></returns>
                    public Children Mutate()
                    {
                        if (new Random().Next(0, 100) < 26)
                            return new Children((1 << 5) ^ Value, Length);

                        return new Children(Value, Length);
                    }
                }
            }
        }
    }
}